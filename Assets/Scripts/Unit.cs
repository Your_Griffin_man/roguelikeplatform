﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    public float maxHP;
    public float maxMP;
    public float hp;
    public float mp;

    public float speed;
    public float jumpSpeed;

    public float damage;
    public float attackSpeed;
    public float attackSpeedValue;

    void Start()
    {
        hp = maxHP;
        mp = maxMP;
        attackSpeedValue = 0;
    }
    
    public void HitMe(float damage)
    {
        hp -= damage;
    }
}