﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Unit
{
    void Start()
    {
        maxHP = 10;
        damage = 2;
        attackSpeed = 1;
        hp = maxHP;
    }
    
    void Update()
    {
        if (hp <= 0)
        {
            GameManager.Instance.PlayerDie();
        }

        if (mp < 0)
            mp = 0;
        else if (mp > maxMP)
            mp = maxMP;

        if (hp < 0)
            hp = 0;
        else if (hp > maxHP)
            hp = maxHP;

        if (attackSpeedValue > 0)
            attackSpeedValue -= Time.deltaTime;
    }
}