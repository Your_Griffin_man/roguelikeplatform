﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
	public Animator animator;

	public Transform attackPoint;
	public float attackRange = 0.5F;
	public LayerMask enemyLayers;
    Player unit;

    private void Start()
    {
        unit = GetComponent<Player>();
    }

    private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Mouse0) && unit.attackSpeedValue <= 0)
		{
			Attack();
		}
	}
	void Attack()
	{
		animator.SetTrigger("Attack");

		Collider2D[] hitEnemys = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);

		foreach(Collider2D enemy in hitEnemys)
		{
            var enemyUnit = enemy.GetComponent<Enemy>();
            enemyUnit.HitMe(unit.damage);

			Debug.Log("Player hit" + enemy.name);
		}

        unit.attackSpeedValue = unit.attackSpeed;
	}
    
	private void OnDrawGizmosSelected()
	{
		if (attackPoint == null)
			return;

		Gizmos.DrawWireSphere(attackPoint.position, attackRange);

	}
}
