﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	public float speed;
	public float jumpForce;
	public float moveInput;

	private Rigidbody2D rb;

	private bool bodyRight = true;

	private bool isGrounded;
	public Transform feetPos;
	public float checkRadius;
	public LayerMask whatIsGround;

	public bool isDoubleJump;
	public int doubleJump;

	public bool isIdle;
	public bool isAttack;
	Animator anim;
	private void Start()
	{
		rb = GetComponent<Rigidbody2D>();
		anim = transform.GetChild(0).GetComponent<Animator>();
	}

	//private void FixedUpdate()
	//{
	//	moveInput = Input.GetAxis("Horizontal");
	//	rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);
	//	if (bodyRight == false && moveInput > 0)
	//	{
	//		Flip();
	//	}
	//	else if (bodyRight == true && moveInput < 0)
	//	{
	//		Flip();
	//	}
	//}
	private void Update()
	{
		isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, whatIsGround);

		if (isGrounded == true && Input.GetKeyDown(KeyCode.Space))
		{
			rb.velocity = Vector2.up * jumpForce;
			doubleJump = 1;
		}
		if (Input.GetKeyDown(KeyCode.F))
		{
			isDoubleJump = !isDoubleJump;
		}
		if (isDoubleJump && doubleJump > 0 && Input.GetKeyDown(KeyCode.Space) && !isGrounded)
		{
			doubleJump--;
			rb.velocity = Vector2.up * jumpForce;

		}

		moveInput = Input.GetAxis("Horizontal");
		rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);
		if (bodyRight == false && moveInput > 0)
		{
			Flip();
		}
		else if (bodyRight == true && moveInput < 0)
		{
			Flip();
		}
		SetAnimation(moveInput == 0);

	}
	void Flip()
	{
		bodyRight = !bodyRight;
		Vector3 Scaler = transform.localScale;
		Scaler.x *= -1;
		transform.localScale = Scaler;
	}

	void SetAnimation(bool isIdle1)
	{
		if(isIdle != isIdle1)
		{
			isIdle = isIdle1;
			anim.SetFloat("walking", isIdle ? 0 : 0.02F);
			anim.SetBool("idle", isIdle);
		}
	}
	void HeroAttack()
	{
		
	}
}
