﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Unit
{
    void Start()
    {
        maxHP = 10;
        hp = maxHP;
    }
    
    void Update()
    {
        if (hp <= 0)
        {
            Destroy(gameObject);
        }

        if (mp < 0)
            mp = 0;
        else if (mp > maxMP)
            mp = maxMP;

        if (hp < 0)
            hp = 0;
        else if (hp > maxHP)
            hp = maxHP;

        if (attackSpeedValue > 0)
            attackSpeedValue -= Time.deltaTime;
    }
}