﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
	public float speed;
	public int positionOfPatrol;
	public Transform point;
	bool moveingRight;

	Transform player;
	public float stoppingDistance;

	bool chill = false;
	bool angry = false;
	bool goBack = false;

	Animator anim;
	bool isInput;

	private void Start()
	{
		anim = GetComponent<Animator>();
		player = GameObject.FindGameObjectWithTag("Player").transform;
	}

	private void Update()
	{
		if (Vector2.Distance(transform.position, point.position) < positionOfPatrol && angry == false)
		{
			chill = true;

		}

		if (Vector2.Distance(transform.position, player.position) < stoppingDistance)
		{
			angry = true;
			chill = false;
			goBack = false;
		}

		if (Vector2.Distance(transform.position, player.position) > stoppingDistance)
		{
			goBack = true;
			angry = false;
		}

		if (chill == true)
		{
			Chill();
		}else if(angry == true)
		{
			Angry();
		}else if(goBack == true)
		{
			GoBack();
		}
		//if (Input.GetKeyDown(KeyCode.Space))
		//{
		//	isInput = !isInput;
		//	anim.SetBool("attack", isInput);
		//}
	}
	void Chill()
	{
		if (transform.position.x > point.position.x + positionOfPatrol)
		{
			moveingRight = false;
		}
		else if (transform.position.x < point.position.x - positionOfPatrol)
		{
			moveingRight = true;
		}
		if (moveingRight)
		{
			transform.position = new Vector2(transform.position.x + speed * Time.deltaTime, transform.position.y);
		}
		else
		{
			transform.position = new Vector2(transform.position.x - speed * Time.deltaTime, transform.position.y);
		}
		
	}

	void Angry()
	{
		transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
		speed = 3;
	}

	void GoBack()
	{
		transform.position = Vector2.MoveTowards(transform.position,point.position, speed * Time.deltaTime);
		speed = 1;
	}
}
